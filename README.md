# Instructions
## Setup
step 1: checkout project, create and checkout new branch (use your name as branch name)

step 2: run npm install to install packages and npm start to run the base project. 

step 3: All required libraries should be installed but feel free to add anything you like

## Stage 1: 
step 1: Make two calls to the following [users API](https://randomuser.me/api?results=500) to retrieve 1000 users, then merge the results  **Use either axios or fetch**, **user uuid is found in login.uuid in the object**

step 2: On a card display the following user information:  combined name (first + middle + last), gender, city, streetName, streetNumber, country, phone number , picture (large) for all the users as picture below.

step 3: Display four users per row **show all of them**

step 4: Add a search bar on the top which should enable search by combined name (first + middle + last)

step 5: Add a toggle button to sort users based on combined name (toggle Asc -> original order)

## Stage 2:
step 1: Store fetched users in the redux store **Already configured**

step 2: Create two pages home page (path:"/") and user page (path: "/{user_uuid}").

step 3: Display users on the home page and create link in the combined name to be able to navigate to the user detail page /{user_uuid}.

step 4: In the user detail page (/{user_uuid}) retrieve the user redux based on the user_uuid then show the message ** WELCOME <combined name> ** else show an error NOT FOUND


**ALL THE REQUIRED LIBRARIES ARE INSTALLED FEEL FREE TO ADD MORE IF REQUIRED**


SAMPLE IMAGE:
        ![img.png](img.png)


